#!/usr/bin/env node

const {join, resolve} = require('path')
const {Edge} = require('./src/edge')
const pino = require('pino')

if (process.argv[2] === undefined) {
  console.error(
    'Specify the path to a configuration file.\n' +
    `See: ${join(__dirname, 'example.config.js')}`
  )
  process.exit(1)
}

const options = require(resolve(process.cwd(), process.argv[2]))

const log = pino()
log.info('Edge starting')
const edge = new Edge(options)
edge.listen().then(() => {
  log.info('Edge started')

  let notify
  try {
    notify = require('sd-notify')
  } catch (error) {
    log.warn('Systemd notifications and heartbeat are unavailable')
  }
  if (notify) {
    const watchdogInterval = notify.watchdogInterval()
    if (watchdogInterval > 0) {
      const interval = Math.max(500, Math.floor(watchdogInterval / 2))
      notify.startWatchdogMode(interval)
    }
    notify.ready()
  }

  process.on('SIGINT', async () => {
    console.log('')
    log.info('Edge stopping')
    await edge.close()
    log.info('Edge stopped')
    process.exit()
  })
})
