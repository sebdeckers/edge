const shellEscape = require('shell-escape')
const {exec} = require('child-process-promise')
const promiseRetry = require('promise-retry')

async function s3cmd (s3, ...args) {
  try {
    return await exec(shellEscape([
      s3.s3cmd,
      `--access_key=${s3.accessKeyId}`,
      `--secret_key=${s3.secretAccessKey}`,
      `--region=${s3.region}`,
      `--host=${s3.endpoint}`,
      `--host-bucket=%(bucket)s.${s3.endpoint}`,
      ...args
    ]))
  } catch (error) {
    const message = error.stderr
      ? error.stderr.replace(/(--\S+_key=)(\S+)/ig, '$1XXXXXXXX')
      : 'Failed to access object storage'
    throw new Error(message)
  }
}

function s3cmdRetry (s3, ...args) {
  return promiseRetry((retry, number) => {
    return s3cmd(s3, ...args)
      .catch(retry)
  })
}

module.exports._s3cmd = s3cmd
module.exports.s3cmd = s3cmdRetry
