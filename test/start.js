require('loud-rejection/register')
const test = require('blue-tape')
const {Edge} = require('..')
const {promisify} = require('util')
const {join} = require('path')
const shellescape = require('shell-escape')
const {exec} = require('child-process-promise')
const exampleConfig = require('../example.config.js')
const {mockBackend} = require('./helpers/mockBackend')
const PubNub = require('pubnub')

const fixture = {
  domain: 'example.net',
  assets: [
    join(__dirname, 'fixtures/public/index.html')
  ]
}

test('Resetting object storage', async (t) => {
  const s3 = {
    s3cmd: exampleConfig.s3.s3cmd,
    region: exampleConfig.s3.region,
    endpoint: exampleConfig.s3.endpoint,
    bucket: exampleConfig.s3.bucket,
    accessKeyId: process.env.COMMONSHOST_CORE_S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.COMMONSHOST_CORE_S3_SECRET_ACCESS_KEY
  }
  try {
    await exec(shellescape([
      s3.s3cmd,
      `--access_key=${s3.accessKeyId}`,
      `--secret_key=${s3.secretAccessKey}`,
      `--region=${s3.region}`,
      `--host=${s3.endpoint}`,
      `--host-bucket=%(bucket)s.${s3.endpoint}`,
      '--recursive',
      '--force',
      'del',
      `s3://${s3.bucket}`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to reset object storage')
  }
  try {
    await exec(shellescape([
      s3.s3cmd,
      `--access_key=${s3.accessKeyId}`,
      `--secret_key=${s3.secretAccessKey}`,
      `--region=${s3.region}`,
      `--host=${s3.endpoint}`,
      `--host-bucket=%(bucket)s.${s3.endpoint}`,
      'put',
      ...fixture.assets,
      `s3://${s3.bucket}/sites/${fixture.domain}/public/`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to prepare object storage')
  }
})

let api
test('Start mock API', async (t) => {
  api = await mockBackend()
  await promisify(api.listen).call(api)
  console.log(`Mock API listening on ${api.address().port}`)
})

let edge
test('Start edge', async (t) => {
  const options = {
    ...exampleConfig,
    core: {api: `https://localhost:${api.address().port}`}
  }
  edge = new Edge(options)
  await edge.listen()
})

let pubnub
const channels = process.env.COMMONSHOST_CORE_PUBNUB_CHANNELS.split(' ')
test('Connect to PubNub', (t) => {
  const subscribeKey = process.env.COMMONSHOST_CORE_PUBNUB_SUBSCRIBE_KEY
  const publishKey = process.env.COMMONSHOST_CORE_PUBNUB_PUBLISH_KEY
  const uuid = process.env.COMMONSHOST_CORE_PUBNUB_UUID
  pubnub = new PubNub({ssl: true, uuid, publishKey, subscribeKey})
  // pubnub.subscribe({channels})
  t.end()
})

test('Delete a site', (t) => {
  pubnub.publish({
    channel: channels[0],
    message: {
      type: 'site-delete',
      domain: fixture.domain
    }
  }, (status, response) => {
    if (status.error === false) t.end()
    else t.end(status)
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 2000))

test('Deploy a site', (t) => {
  pubnub.publish({
    channel: channels[0],
    message: {
      type: 'site-deploy',
      domain: fixture.domain,
      isNewDomain: true,
      hasNewFiles: true,
      hasNewConfiguration: true
    }
  }, (status, response) => {
    if (status.error === false) t.end()
    else t.end(status)
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 2000))

test('Delete a file', (t) => {
  pubnub.publish({
    channel: channels[0],
    message: {
      type: 'file-delete',
      domain: fixture.domain,
      files: ['/index.html']
    }
  }, (status, response) => {
    if (status.error === false) t.end()
    else t.end(status)
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 2000))

test('Update a file', (t) => {
  pubnub.publish({
    channel: channels[0],
    message: {
      type: 'file-update',
      domain: fixture.domain,
      files: ['/index.html']
    }
  }, (status, response) => {
    if (status.error === false) t.end()
    else t.end(status)
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 2000))

test('Stop pubnub', async (t) => pubnub.stop())
test('Stop edge', async (t) => edge.close())
test('Stop mock API', (t) => api.close(t.end))
