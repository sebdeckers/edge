# edge 🌟

Keeps content, certificates, and configurations on the frontend servers synced with the backend stores.

## Joining the Network

Sync S3 bucket to local directory.

Retrieve configurations for all sites.

Rebuild master configuration. Skip sites with invalid configurations. Skip sites without crypto.

Subscribe to PubNub channel to start receiving notifications from core.

## Content Updates

### Bulk

- Receive a `deploy` message.
- Sync S3 bucket to local directory.
- Trigger re-indexing of the domain in @commonshost/server.

### Atomic

TBD. Probably receive `deploy` message with deltas: which files were added, changed, moved, or deleted.

## Configuration Updates

- Expire existing host options/manifest.
- Rebuild and apply updated configuration.

## Certificate Updates

- Receive an `acme` message.
- Retrieve new certs from S3 bucket.
- WIP: Read CN/SAN from cert to use as coalesced connection (alt svc, origin set).
